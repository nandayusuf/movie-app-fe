import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './component/header/Header';
import Home from './container/Home';
import MovieDetail from './container/MovieDetail';
import Register from './container/Register';
import Checkout from './container/Checkout';
import Category from './container/Category';
import Membership from './container/Membership';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Transaction from './container/Transaction';
import ScrollToTop from './component/ScrollToTop/ScrollToTop';

function App() {
  const userData = useSelector(state => state.user);
  return (
    <Router>
      <div className="App">
        <Header />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/detail/:id">
            <MovieDetail />
          </Route>
          <Route exact path="/category/:category">
            <Category />
          </Route>
          <Route exact path="/register">
            <Register />
          </Route>
          <Route exact path="/checkout">
            { userData.isLogin ? <Checkout /> :  <Redirect to='/' />}
          </Route>
          <Route exact path="/membership">
            { userData.isLogin ? <Membership /> :  <Redirect to='/' />}
          </Route>
          <Route exact path="/transaction/:id">
            <Transaction />
          </Route>
        </Switch>
        <ScrollToTop />
      </div>
    </Router>
  );
}

export default App;
