import { useState } from 'react';
import axios from 'axios';
import { Container, Table, Image } from 'react-bootstrap';
import { useParams } from "react-router-dom";
import './Transaction.scss';
import { renderPrice } from '../lib/moneyGenerator';

const Transaction = () => {
  const { id } = useParams();
  const [discount, setDiscount] = useState(0);
  const [total, setTotal] = useState(0);
  const [totalItem, setTotalItem] = useState(0);
  const [item, setItem] = useState([]);
  const [request, setRequest] = useState(false);
  const [date, setDate] = useState("");

  const getTransaction = () => {
    setRequest(true);
    axios.get(`${process.env.REACT_APP_BASE_URL}/transaction`, {
      params: {
        "id_transaction": id
      }
    }).then(res => {
      const dataTransaction = res.data;
      setDiscount(res.data.discount);
      setItem(dataTransaction.items);
      setDate(res.data.created_at);
      let tmpTotalItem = 0;
      let tmpTotal = 0;
      dataTransaction.items.forEach(data => {
        tmpTotalItem += data.qty;
        tmpTotal += (data.qty * data.item.price)
      });
      setTotalItem(tmpTotalItem);
      setTotal(tmpTotal);
    }).catch(err => console.log(err));
  }

  const renderTable = () => {
    if (!request) getTransaction();
    return item.map(data => {
      return (
        <tr key={ data.item.id }>
          <td>
            <Image src={ data.item.image } rounded />
            <b>{ data.item.title }</b>
          </td>
          <td className="align-middle">
            { data.qty }
          </td>
          <td className="align-middle">{ renderPrice(data.item.price) }</td>
          <td className="align-middle">{ renderPrice(data.item.price * data.qty) }</td>
        </tr>
      )
    })
  }

  const renderDiscount = () => {
    return (
      <tr>
        <td>
          <b>Membership discount ( { discount }%)</b>
        </td>
        <td></td>
        <td></td>
        <td>{ renderPrice(total * (discount / 100)) }</td>
      </tr>
    )
  }

  const renderDate = () => {
    const dateTime = new Date(date);
    dateTime.setHours(dateTime.getHours() + 7);
    return `${dateTime.getDate()}-${dateTime.getMonth() + 1}-${dateTime.getFullYear()} ${dateTime.getHours()}:${dateTime.getMinutes()}`;
  }

  return (
    <Container id="transaction">
      <h3>Transaction Report</h3>
      <div className="d-flex justify-content-between">
        <span>ID Transaction: {id}</span>
        <span>Time Order: { date === "" ? "01-01-1970" : renderDate() }</span>
      </div>
      <Table bordered variant="dark">
      <thead>
          <tr>
            <th>Item</th>
            <th>Qty</th>
            <th>Price</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          { renderTable() }
        <tr>
          <td>
            <b>Subtotal</b>
          </td>
          <td>{ totalItem }</td>
          <td></td>
          <td>{ renderPrice(total) }</td>
        </tr>
          { discount > 0 ? renderDiscount() : null }
        <tr>
          <td>
            <b>Total</b>
          </td>
          <td></td>
          <td></td>
          <td>{ renderPrice(total - (total * (discount / 100))) }</td>
        </tr>
        </tbody>
      </Table>
    </Container>
  )
}

export default Transaction;