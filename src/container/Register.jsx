import { Container, Form, Button } from "react-bootstrap";
import { useState } from "react";
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { setUser } from '../redux/action/userAction';
import { useHistory } from 'react-router-dom';

const Register = () => {
  const [forms, setForms] = useState({});
  const [errors, setErrors] = useState({});
  const dispatch = useDispatch();
  let history = useHistory();

  const setField = (field, value) => {
    setForms({
      ...forms,
      [field]: value
    });
    if (!!errors[field]) setErrors({
      ...errors,
      [field]: null
    })
  }

  const findError = () => {
    const password = forms.password;
    const confirm = forms.confirm;
    const newErrors = {}
    if (password !== confirm) newErrors.confirm = "Password not match"
    return newErrors;
  }

  const handleSubmit = e => {
    e.preventDefault();
    const newError = findError();

    if (Object.keys(newError).length > 0) {
      setErrors(newError)
    } else {
      axios.post(`${process.env.REACT_APP_BASE_URL}/register`, {
        name: forms.name,
        email: forms.email,
        password: forms.password
      }).then(res => {
        dispatch(setUser(res.data));
        localStorage.setItem('token', res.data.token);
        history.push('/');
      }).catch(err => {
        console.log(err);
        if (err.response.status === 401) {
          alert('e-mail address already registered');
        } else if (err.response.status === 500) {
          alert("Can't insert data");
        }
      });
    }
  }

  return (
    <section id="register">
      <Container>
        <Form onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="full-name">
          <Form.Label>Full Name</Form.Label>
            <Form.Control type="text" placeholder="Enter Your Name" onChange={ e => setField('name', e.target.value) } required />
        </Form.Group>
          
        <Form.Group className="mb-3" controlId="email">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" onChange={ e => setField('email', e.target.value) } required />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Enter Password" onChange={ e => setField('password', e.target.value) } required />
          </Form.Group>
          
        <Form.Group className="mb-3" controlId="confirm-password">
          <Form.Label>Confirm Password</Form.Label>
            <Form.Control type="password" placeholder="Type again your password" onChange={e => setField('confirm', e.target.value)} isInvalid={!!errors.confirm} required />
            <Form.Control.Feedback type='invalid'>{ errors.confirm }</Form.Control.Feedback>
        </Form.Group>
          <Button variant="primary" type="submit">Submit</Button>
        </Form>
      </Container>
    </section>
  )
}

export default Register;