import { useEffect } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import MovieCard from '../component/movie/MovieCard';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { setMovie } from '../redux/action/movieAction';
import { useParams } from "react-router-dom";

const Category = () => {
  const { category } = useParams();
  const dispatch = useDispatch();
  const movieData = useSelector(state => state.movie);

  const capitalize = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  const renderMovie = () => {
    const categoryMovie = movieData.allMovie.filter(data => {
      return data.genre.includes(capitalize(category));
    });
    return categoryMovie.map(data => {
      return <Col key={data.id} className="mb-3"><MovieCard movieData={ data } /></Col>
    })
  }

  useEffect(() => {
    if (movieData.allMovie.length < 1) {
      axios.get(`${process.env.REACT_APP_BASE_URL}/all-movie`).then(res => {
        dispatch(setMovie(res.data));
      }).catch(err => console.log(err));
    }
  });

  return (
    <section id="home">
      <Container>
        <h2 className="text-center">{ capitalize(category) } Movie</h2>
        <Row>
          { renderMovie() }
        </Row>
      </Container>
    </section>
  )
}

export default Category;