import { useEffect } from "react";
import { Row, Col, Image, Button, Container } from "react-bootstrap";
import Trailer from '../component/movie/Trailer';
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import axios from 'axios';
import { setMovie, addMovieToCart, updateCart } from '../redux/action/movieAction';
import { renderPrice } from '../lib/moneyGenerator';

const MovieDetail = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const movieData = useSelector(state => state.movie);
  const movieFilter = movieData.allMovie.find(data => {
    return data.id === id;
  });

  const renderGenre = () => {
    return movieFilter.genre.join(", ");
  };

  const orderMovie = () => {
    const cartData = movieData.cart;
    const testMovie = cartData.find(data => {
      return data.item.id === id;
    });
    if (testMovie) {
      const cartIndex = cartData.indexOf(testMovie);
      cartData[cartIndex].qty += 1;
      dispatch(updateCart(cartData));
    } else {
      dispatch(addMovieToCart(movieFilter));
    }
  }

  useEffect(() => {
    if (movieData.allMovie.length < 1) {
      axios.get('http://localhost:5000/all-movie').then(res => {
        dispatch(setMovie(res.data));
      }).catch(err => console.log(err));
    }
  });

  return (
    <section id="movie-detail">
      <Container>
        <Row className="mb-5">
          <Col md={ 2 }>
            <Image src={ movieFilter ? movieFilter.image : "" } rounded className="w-100" />
          </Col>
          <Col className="align-middle">
            <div className="text-left my-3">
              <h3>{ movieFilter ? movieFilter.title : "" }</h3>
              <p>Genre: { movieFilter ? renderGenre() : "" }</p>
              <p>Price: {renderPrice(movieFilter ? movieFilter.price : 0)}</p>
              <Button onClick={ orderMovie }>Order Now</Button>
            </div>
          </Col>
        </Row>
        <div className="overview text-left">
          <h3>Overview</h3>
          <p>{ movieFilter ? movieFilter.description : ""}</p>
        </div>
        <div className="mt-3 text-left">
          <h3>Trailer</h3>
          <Trailer embedId={ movieFilter ? movieFilter.trailer : "UaXopiVAsTY"} />
        </div>
      </Container>
    </section>
  )
}

export default MovieDetail;