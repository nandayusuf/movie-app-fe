import { useEffect } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import MovieCard from '../component/movie/MovieCard';
import MovieCarousel from '../component/movie/MovieCarousel';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { setMovie } from '../redux/action/movieAction';

const Home = () => {
  const dispatch = useDispatch();
  const movieData = useSelector(state => state.movie);

  const renderMovie = () => {
    return movieData.allMovie.map(data => {
      return <Col key={data.id} className="mb-3"><MovieCard movieData={data} /></Col>
    });
  }

  useEffect(() => {
    if (movieData.allMovie.length < 1) {
      axios.get(`${process.env.REACT_APP_BASE_URL}/all-movie`).then(res => {
        dispatch(setMovie(res.data));
      }).catch(err => console.log(err));
    }
  });

  return (
    <section id="home">
      <Container>
        <MovieCarousel />
        <Row>
          { renderMovie() }
        </Row>
      </Container>
    </section>
  )
}

export default Home;