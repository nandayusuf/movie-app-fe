import { useState } from 'react';
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { setMembershipData, setUserMembership } from '../redux/action/userAction';
import './Membership.scss';

const Membership = () => {
  const membershipData = useSelector(state => state.user.membership);
  const userData = useSelector(state => state.user.data);
  const dispatch = useDispatch();
  const [membership, setMembership] = useState(0);
  const history = useHistory()

  const renderMembership = () => {
    if (Object.keys(membershipData).length > 0) {
      const basicMember = [
        <Col key="0" className="my-2" md={ 6 }>
            <label>
              <input type="radio" name="demo" id="demo-0" checked={ membership === null || membership === 0 } onChange={e => setMembership(null)} className="d-none card-input-element" value="0" />
              <Card style={{ width: '18rem' }} bg="dark">
                <Card.Body>
                  <Card.Title className="text-capitalize">Basic</Card.Title>
                  <Card.Text>Basic Membership</Card.Text>
                </Card.Body>
              </Card>
            </label>
          </Col>
      ]
      const canSubscribe = membershipData.canSubscribe.map(data => data.id);
      if (membership === 0 && userData.membership) setMembership(userData.membership);
      const allMembership = membershipData.allMembership.map(data => {
        return (
          <Col key={data.id} className="my-2" md={ 6 }>
            <label>
              <input type="radio" name="demo" id={"demo-" + data.id} checked={data.id === membership} disabled={!canSubscribe.includes(data.id)} onChange={e => setMembership(parseInt(e.target.value))} className="d-none card-input-element" value={ data.id } />
              <Card style={{ width: '18rem' }} bg="dark">
                <Card.Body>
                  <Card.Title className="text-capitalize">{ data.name }</Card.Title>
                  <Card.Text>{ data.description }</Card.Text>
                </Card.Body>
              </Card>
            </label>
          </Col>
        )
      })
      return allMembership.concat(basicMember);
    } else {
      axios.get(`${process.env.REACT_APP_BASE_URL}/membership`, {
        headers: {
          Authorization: 'Bearer ' + userData.token
        }
      }).then(res => {
        dispatch(setMembershipData(res.data));
      }).catch(err => console.log(err));
    }
  }

  const handleSubmit = event => {
    event.preventDefault();
    if (membership !== userData.membership && membership !== 0) {
      axios.post(`${process.env.REACT_APP_BASE_URL}/subscribe`, {
        id_membership: membership
      }, {
        headers: {
          Authorization: 'Bearer ' + userData.token
        }
      }).then(res => {
        alert('Subscribtion sucess');
        dispatch(setUserMembership(membership));
        history.push('/');
      }).catch(err => console.log(err));
    } else {
      alert("Can't Subscribe to current membership");
    }
  }

  return (
    <Container onSubmit={ handleSubmit }>
      <h2>Subscribe Membership</h2>
      <Form>
        <Row className="my-3">
           { renderMembership() }
        </Row>
        <Button type="submit">Subscribe</Button>
      </Form>
    </Container>
  )
}

export default Membership;