import { useState } from 'react';
import { Container, Image, Table, Button, Form } from "react-bootstrap";
import axios from 'axios';
import { Trash } from 'react-bootstrap-icons';
import { useSelector, useDispatch } from 'react-redux';
import { updateCart, emptyCart } from '../redux/action/movieAction';
import { setMembershipData } from '../redux/action/userAction';
import { useHistory } from 'react-router-dom';
import './Checkout.scss';
import { renderPrice } from '../lib/moneyGenerator';

const Checkout = () => {
  const cartData = useSelector(state => state.movie.cart);
  const userData = useSelector(state => state.user.data);
  const membershipData = useSelector(state => state.user.membership);
  const [discount, setDiscount] = useState(0);
  const dispatch = useDispatch();
  const history = useHistory();

  const renderTable = () => {
    return cartData.map(data => {
      return (
        <tr key={ data.item.id }>
          <td>
            <Image src={ data.item.image } rounded />
            <b>{ data.item.title }</b>
          </td>
          <td className="align-middle">
            <Form.Control size="sm" type="number" value={data.qty} onChange={ e => updateQty(data, e.target.value)}></Form.Control>
          </td>
          <td className="align-middle">{ renderPrice(data.item.price) }</td>
          <td className="align-middle">{ renderPrice(data.item.price * data.qty) }</td>
          <td className="align-middle"><Button variant="danger" onClick={ () => deleteItem(data) }><Trash /></Button></td>
        </tr>
      )
    })
  }

  const getTotal = () => {
    let total = 0;
    cartData.forEach(data => {
      total += data.item.price * data.qty;
    });
    return total;
  }
  const getFinalTotal = () => {
    let total = 0;
    cartData.forEach(data => {
      total += data.item.price * data.qty;
    });
    return total - (total * (discount / 100));
  }

  const getTotalItem = () => {
    let total = 0;
    cartData.forEach(data => {
      total += parseInt(data.qty);
    });
    return total;
  }

  const deleteItem = data => {
    const itemIndex = cartData.indexOf(data);
    cartData.splice(itemIndex, 1);
    if (cartData.length > 0) {
      localStorage.setItem('cart', JSON.stringify(cartData));
    } else {
      localStorage.removeItem('cart');
    }
    dispatch(updateCart(cartData));
  }

  const updateQty = (item, qty) => {
    if (qty > 0 && qty < 1000) {
      const itemIndex = cartData.indexOf(item);
      cartData[itemIndex].qty = parseInt(qty);
      dispatch(updateCart(cartData));
    }
  }

  const processCheckout = () => {
    axios.post(`${process.env.REACT_APP_BASE_URL}/checkout`, {
      items: cartData,
      discount: discount
    }, {
      headers: {
        Authorization: 'Bearer ' + userData.token
      }
    }).then(res => {
      alert('Checkout Success');
      dispatch(emptyCart());
      localStorage.removeItem('cart');
      axios.get(`${process.env.REACT_APP_BASE_URL}/membership`, {
        headers: {
          Authorization: 'Bearer ' + userData.token
        }
      }).then(response => {
        dispatch(setMembershipData(response.data));
      }).catch(error => console.log(error));
      history.push(`/transaction/${res.data.id_transaction}`);
    }).catch(err => console.log(err))
  }

  const renderDiscount = () => {
    if ('canSubscribe' in membershipData && userData.membership) {
      const userMembership = membershipData.canSubscribe.filter(data => {
        return data.id === userData.membership;
      });
      if (discount !== userMembership[0].discount) setDiscount(userMembership[0].discount)
      return userMembership.map(data => {
        return (
          <tr key={ data.id }>
            <td>
              <b>Membership {data.name} discount ( { data.discount }%)</b>
            </td>
            <td></td>
            <td></td>
            <td>{ renderPrice(getTotal() * (discount / 100))}</td>
            <td></td>
          </tr>
        )
      })
    }
  }

  return (
    <Container id="checkout">
      <h3>Shopping Cart</h3>
      <h5>Order Detail</h5>
      <Table bordered variant="dark">
        <thead>
          <tr>
            <th>Item</th>
            <th>Qty</th>
            <th>Price</th>
            <th>Total</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          { renderTable() }
          <tr>
            <td>
              <b>Subtotal</b>
            </td>
            <td>{ getTotalItem() }</td>
            <td></td>
            <td>{renderPrice(getTotal())}</td>
            <td></td>
          </tr>
          { renderDiscount() }
          <tr>
            <td>
              <b>Total</b>
            </td>
            <td></td>
            <td></td>
            <td>{renderPrice(getFinalTotal())}</td>
            <td></td>
          </tr>
        </tbody>
      </Table>
      <Button onClick={processCheckout} disabled={ cartData.length < 1 }>Checkout</Button>
    </Container>
  )
}

export default Checkout;