import ActionType from "../constant/global-action-type";

export const setMovie = data => {
  return {
    type: ActionType.SET_MOVIE,
    data: data
  }
}
export const addMovieToCart = data => {
  return {
    type: ActionType.ADD_MOVIE_TO_CART,
    data: {
      item: data,
      qty: 1
    }
  }
}

export const updateCart = data => {
  return {
    type: ActionType.UPDATE_CART,
    data: data
  }
}

export const emptyCart = () => {
  return {
    type: ActionType.EMPTY_CART
  }
}

export const setCart = data => {
  return {
    type: ActionType.SET_CART,
    data: data
  }
}