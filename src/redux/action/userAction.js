import ActionType from "../constant/global-action-type";

export const setUser = (user) => {
  return {
    type: ActionType.SET_USER,
    data: user
  }
}

export const Logout = () => {
  return {
    type: ActionType.LOGOUT
  }
}

export const setMembershipData = data => {
  return {
    type: ActionType.GET_MEMBERSHIP_DATA,
    data: data
  }
}

export const setUserMembership = data => {
  return {
    type: ActionType.SET_USER_MEMBERSHIP,
    data: data
  }
}