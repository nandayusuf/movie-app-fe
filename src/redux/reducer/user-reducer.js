import ActionType from '../constant/global-action-type';

const userState = {
  isLogin: false,
  data: {},
  membership: {}
}

const UserReducer = (state = userState, action) => {
  switch (action.type) {
    case ActionType.LOGOUT:
      return ({
        ...state,
        isLogin: false,
        data: {},
        membership: {}
      })
    case ActionType.SET_USER:
      return ({
        ...state,
        isLogin: true,
        data: action.data
      })
    case ActionType.GET_MEMBERSHIP_DATA:
      return ({
        ...state,
        membership: action.data
      })
    case ActionType.SET_USER_MEMBERSHIP:
      return ({
        ...state,
        data: {
          ...state.data,
          membership: action.data
        }
      })
    default:
      return state;
  }
}

export default UserReducer;