import { combineReducers } from "redux";
import MovieReducer from './movie-reducer';
import UserReducer from './user-reducer';

const reducers = combineReducers({
  movie: MovieReducer,
  user: UserReducer
});

export default reducers;