import ActionType from '../constant/global-action-type';

const movieState = {
  allMovie: [],
  cart: []
}

const MovieReducer = (state = movieState, action) => {
  switch (action.type) {
    case ActionType.SET_MOVIE:
      return {
        ...state,
        allMovie: action.data
      };
    case ActionType.ADD_MOVIE_TO_CART:
      return {
        ...state,
        cart: [...state.cart, action.data]
      }
    case ActionType.UPDATE_CART:
      return {
        ...state,
        cart: [...state.cart.slice(0, action.data+1), ...state.cart.slice(action.data+2)]
      }
    case ActionType.EMPTY_CART:
      return {
        ...state,
        cart: []
      }
    case ActionType.SET_CART:
      return {
        ...state,
        cart: action.data
      }
    default:
      return state;
  }
}

export default MovieReducer;