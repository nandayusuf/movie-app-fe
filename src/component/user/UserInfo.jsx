import { NavDropdown } from "react-bootstrap";
import { Logout } from "../../redux/action/userAction";
import { emptyCart } from '../../redux/action/movieAction';
import { useDispatch } from "react-redux";
import "./UserInfo.scss";

const UserInfo = props => {
  const dispatch = useDispatch();

  const actionLogout = () => {
    localStorage.removeItem('token');
    dispatch(Logout());
    dispatch(emptyCart());
    localStorage.removeItem('cart');
  }
  return (
    <NavDropdown title={ props.name } id="basic-nav-dropdown">
      <NavDropdown.Item href="#" onClick={ actionLogout}>Logout</NavDropdown.Item>
    </NavDropdown>
  );
}

export default UserInfo;