import { Modal, Button, Container, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { setUser } from '../../redux/action/userAction';
import { useDispatch } from 'react-redux';
import './LoginModal.scss';

const LoginModal = props => {
  const dispatch = useDispatch();

  const handleSubmit = async (event) => {
    const form = event.currentTarget;
    event.preventDefault();
    if (form.checkValidity() === false) {
      event.stopPropagation();
    } else {
      axios.post('http://localhost:5000/login', {
        email: event.target.email.value,
        password: event.target.password.value
      }).then(res => {
        dispatch(setUser(res.data));
        localStorage.setItem('token', res.data.token);
        props.onHide();
      }).catch(err => {
        console.log(err);
        if (err.response.status === 401) {
          alert('Wrong Username or password');
        } else {
          alert('Please try again');
        }
      })
    }
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Login
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid">
        <Container>
          <Form onSubmit={handleSubmit}>
          <Form.Group className="mb-3" controlId="email">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" required placeholder="Enter email" />
          </Form.Group>
          <Form.Group className="mb-3" controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" required placeholder="Enter Password" />
            </Form.Group>
            <Button variant="primary" type="submit">Login</Button>
          </Form>
          <p className="mt-3">Don't have account? <Link to="/register" onClick={ props.onHide }>Click Here</Link> to register</p>
        </Container>
      </Modal.Body>
    </Modal>
  )
}

export default LoginModal;