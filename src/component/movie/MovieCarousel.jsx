import { Carousel } from "react-bootstrap";
import { Link } from 'react-router-dom';
import './MovieCarousel.scss';
import { useSelector } from "react-redux";

const MovieCarousel = () => {
  // 1100 x 500
  const movieData = useSelector(state => state.movie);
  const renderCarouselItem = () => {
    let output = [];
    const carouselLength = movieData.allMovie.length > 3 ? 3 : movieData.allMovie.length;
    for (let i = 0; i < carouselLength; i++) {
      output.push(
        <Carousel.Item interval={1000} key={ movieData.allMovie[i].id }>
          <Link to={ "/detail/"+movieData.allMovie[i].id }>
            <img
              className="d-block w-100"
              src={movieData.allMovie[i].image}
              alt={ movieData.allMovie[i].title }
            />
            <Carousel.Caption>
              <h3>{ movieData.allMovie[i].title }</h3>
            </Carousel.Caption>
          </Link>
        </Carousel.Item>
      )
    }
    return output;
  }
  return (
    <Carousel>
      { renderCarouselItem() }
    </Carousel>
  )
}

export default MovieCarousel;