import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './MovieCard.scss';
import { renderPrice } from '../../lib/moneyGenerator';

const MovieCard = props => {
  return (
    <Link to={ "/detail/"+props.movieData.id } className="movie-card-link">
      <Card style={{ width: '11rem' }} className="movie-card">
        <Card.Img variant="top" src={ props.movieData.image } className="rounded" />
        <Card.Body className="d-flex justify-content-between flex-column">
          <Card.Title>{props.movieData.title}</Card.Title>
          <div className="price d-flex justify-content-between">
            <span>Price:</span>
            <span>{ renderPrice(props.movieData.price) }</span>
          </div>
        </Card.Body>
        </Card>
      </Link>
  )
}

export default MovieCard;