import { Button } from "react-bootstrap";
import { ArrowUp } from "react-bootstrap-icons";
import { useState, useEffect } from "react";
import './ScrollToTop.scss';

const ScrollToTop = () => {
  const [show, setShow] = useState(false);

  const ScrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    })
  }

  const CheckScrollPos = data => {
    if (data > 100) {
      if (!show) setShow(true);
    } else {
      if (show) setShow(false);
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', () => {
      var prevScrollPos = window.pageYOffset
      CheckScrollPos(prevScrollPos)
    })
  })

  return (
    <Button className={ show ? "scroll-to-top rounded-pill px-2 showScrollBtn" : "scroll-to-top rounded-pill px-2" } onClick={ScrollToTop}><ArrowUp /></Button>
  )
}

export default ScrollToTop;