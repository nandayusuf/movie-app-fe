import { useEffect, useState } from "react";
import { Col, Dropdown, NavItem, NavLink, Row, Button, Badge } from "react-bootstrap";
import { Cart3 } from 'react-bootstrap-icons';
import { useHistory } from 'react-router-dom';
import './Cart.scss';
import { useSelector, useDispatch } from 'react-redux';
import { setCart } from '../../redux/action/movieAction';

const Cart = props => {
  const cartData = useSelector(state => state.movie.cart);
  const userData = useSelector(state => state.user);
  let history = useHistory();
  const dispatch = useDispatch();
  const [totalItem, setTotalItem] = useState(0);

  const renderCart = () => {
    if (cartData.length > 0) localStorage.setItem('cart', JSON.stringify(cartData));
    let total = 0;
    const output = cartData.map(data => {
      total += data.qty;
      return (
        <Row className="item" key={ data.item.id }>
          <Col md={ 8 } xs={ 8 }>{ data.item.title }</Col>
          <Col md={ 3 } xs={ 3 }>{ data.qty }</Col>
        </Row>
      )
    })
    if (totalItem !== total) setTotalItem(total);
    return output;
  }

  const processCheckout = () => {
    if (userData.isLogin) {
      document.body.click();
      history.push('/checkout');
    } else {
      props.showLogin();
    }
  }

  useEffect(() => {
    const cart = localStorage.getItem('cart');
    if (cart && cartData.length === 0) {
      const localCart = JSON.parse(cart);
      dispatch(setCart(localCart));
    }
  })

  return (
    <Dropdown as={NavItem} id="cart" className="ml-auto">
      <Dropdown.Toggle as={NavLink}><Cart3 />{ totalItem > 0 ? <Badge bg="secondary">{ totalItem }</Badge> : null }</Dropdown.Toggle>
      <Dropdown.Menu className="text-center bg-dark text-white">
        <Row className="header">
          <Col md={ 8 } xs={ 8 }>Title</Col>
          <Col md={ 3 } xs={ 3 }>Qty</Col>
        </Row>
        { renderCart() }
        <Button onClick={() => processCheckout()} disabled={ cartData.length < 1 }>Checkout</Button>
      </Dropdown.Menu>
    </Dropdown>
  )
}

export default Cart;