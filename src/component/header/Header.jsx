import { useState, useEffect } from 'react';
import './Header.scss';
import { Container, Nav, Navbar, Button } from 'react-bootstrap';
import { Link, useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import LoginModal from '../user/LoginModal';
import UserInfo from '../user/UserInfo';
import Cart from './Cart';
import { setUser, setMembershipData } from '../../redux/action/userAction';
import axios from 'axios';

const Header = () => {
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();
  const [loginModal, setLoginModal] = useState(false);
  const [shadow, setShadow] = useState(false);
  const location = useLocation();
  const [category, setCategory] = useState("");

  const checkMembership = () => {
    if (Object.keys(user.membership).length > 0) {
      if (user.membership.canSubscribe) {
        if (user.membership.canSubscribe.length > 0) {
          return true;
        }
      }
    }
    return false;
  }

  const checkScrollPos = data => {
    if (data > 100) {
      if (!shadow) setShadow(true);
    } else {
      if (shadow) setShadow(false);
    }
  }

  const checkPath = (pathSplit) => {
    if (pathSplit.length === 2) {
      if (pathSplit[0] === "category" && category !== pathSplit[1]) {
        setCategory(pathSplit[1]);
      } else {
        setCategory("");
      }
    } else if (pathSplit.length === 1) {
      if (pathSplit[0] === 'membership' && category !== pathSplit[0]) {
        setCategory(pathSplit[0]);
      } else {
        setCategory("");
      }
    }
  }

  useEffect(() => {
    const pathSplit = location.pathname.split('/');
    pathSplit.shift();
    if (!pathSplit.includes(category)) {
      checkPath(pathSplit);
    }
    window.addEventListener('scroll', () => {
      const prevScrollPos = window.pageYOffset;
      checkScrollPos(prevScrollPos);
    })

    const token = localStorage.getItem('token');
    if (!user.isLogin && token) {
      axios.get(`${process.env.REACT_APP_BASE_URL}/user`, {
        headers: {
          Authorization: 'Bearer ' + token
        }
      }).then(res => {
        let data = res.data;
        data.token = token
        dispatch(setUser(res.data))
      }).catch(err => {
        console.log(err);
        localStorage.removeItem('token');
      });
    } else if (token && Object.keys(user.membership).length === 0) {
      axios.get(`${process.env.REACT_APP_BASE_URL}/membership`, {
        headers: {
          Authorization: 'Bearer ' + token
        }
      }).then(res => {
        dispatch(setMembershipData(res.data))
      }).catch(err => console.log(err))
    }
  });

  return (
    <section id="header">
      <Navbar bg="dark" expand="lg" fixed="top" className={ shadow ? "shadow" : null }>
        <Container>
          <Navbar.Brand as={ Link } to="/" >Movie App</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              { user.isLogin && checkMembership() ? <Nav.Link as={ Link } to="/membership" className={ category === 'membership' ? 'active': null }>Membership</Nav.Link> : null }
              <Nav.Link as={Link} to="/category/action" className={ category === 'action' ? 'active': null }>Action</Nav.Link>
              <Nav.Link as={ Link } to="/category/adventure" className={ category === 'adventure' ? 'active': null }>Adventure</Nav.Link>
              <Nav.Link as={ Link } to="/category/horror" className={ category === 'horror' ? 'active': null }>Horror</Nav.Link>
            </Nav>
            <Cart showLogin={ () => setLoginModal(true)} />
            { user.isLogin ? <UserInfo name={ user.data.name } /> : <Button variant="outline-primary" onClick={() => setLoginModal(true)} id="login-btn">Login</Button> }
          </Navbar.Collapse>
        </Container>
        <LoginModal show={loginModal} onHide={() => setLoginModal(false)} />
      </Navbar>
    </section>
  )
}

export default Header;