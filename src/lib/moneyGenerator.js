export const renderPrice = (amount) => {
  const tmp = amount.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&.');
  const tmp1 = tmp.split('.');
  tmp1.pop();
  const tmp2 = tmp1.join('.');
  return 'Rp. ' + tmp2;
}